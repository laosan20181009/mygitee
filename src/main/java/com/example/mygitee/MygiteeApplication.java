package com.example.mygitee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MygiteeApplication {

    public static void main(String[] args) {
        SpringApplication.run(MygiteeApplication.class, args);
    }

}
